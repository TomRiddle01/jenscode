from turtle import *
from math import *
import time
import numpy

r = min(window_height(), window_width())/2-20
size = 3 #radius_x = radius_(x+1)/size -> verhältnis der größen der kreise

circles = [r/size**x for x in range(6)]+[0]
ht()




def drawCircle(radius, center, t, v, d):
    x,y = center
    up()
    goto(x, y-radius)
    down()
    circle(radius)
    
    if radius < r:
        up()
        goto(x+cos(t*v*d)*radius, y+sin(t*v*d)*radius)
        dot(10)
        down()


st = time.time()

line = Turtle()
line.up()
while True:
    t = time.time()-st

    tracer(0)
    clear()

    offset = (0,0)
    for i in range(len(circles)-1):
        radius = circles[i]
        next_radius = circles[i+1]

        v = circles[0]/radius

        pre = offset
        direction = 1 if i%2==0 else -1
        drawCircle(radius, offset, t, v, direction)
        offset = (offset[0]+cos(pi*0.5+direction*t*v)*(radius-next_radius), \
                  offset[1]+sin(pi*0.5+direction*t*v)*(radius-next_radius))


    line.goto(pre)
    line.color("red")
    line.down()
    tracer(1)



exitonclick()
