from math import *
from turtle import *
import time

radius=200

dotTurtle = Turtle()
dotTurtle.color("black")

lineTurtle = Turtle()
lineTurtle.color("red")

def drawCircle():
    tracer(False)
    #kreis
    up()
    goto(0,-radius)
    down()
    circle(radius)
    tracer(True)
    

def makeThis(n, f, tracing=True):
    tracer(False)
    nodes = []
    for i in range(0, n):
        #falsche durchlaufrichtung
        #skalierungsfaktor in liste
        x = cos((2*pi/n)*i+pi)
        y = -sin((2*pi/n)*i+pi)
        nodes.append((x*radius, y*radius))
        dotTurtle.up()
        dotTurtle.goto(nodes[i])
        dotTurtle.down()
        dotTurtle.dot(8)
    tracer(tracing)
    for i in range(0, n):
        x1, y1 = nodes[int(i % n)]
        lineTurtle.up()
        lineTurtle.goto(x1,y1)
        lineTurtle.down()
        x2, y2 = nodes[int((i*f) % n)]
        lineTurtle.goto(x2,y2)
        
        # zeichne linie p1 zu p2

def makeMultiple(n, f):
    if n < 3: n = 3
    for k in range(3, n):
        tracer(False)
        dotTurtle.clear()
        lineTurtle.clear()
        makeThis(k, f, False)
        tracer(True)
        time.sleep(0.01)





# Animationen an sich 

def wobble():
    drawCircle()
    for f in range(0, 10):
        makeMultiple(150, f)

def floating():
    drawCircle()
    f = 0
    while(f <= 10):
        tracer(False)
        dotTurtle.clear()
        lineTurtle.clear()
        makeThis(200, f, False)
        tracer(True)
        time.sleep(0.01)
        f+=0.1

floating()
wobble()

tracer(True)
time.sleep(20)
