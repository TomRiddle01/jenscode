from math import factorial, ceil
from turtle import *
import time

s = time.time()
h = 4096

tab = []
for i in range(h):
    a = []
    tab.append(a)
    for i in range(h):
        a.append(0)

tab[0][0] = 1
tab[1][0] = 1
tab[1][1] = 1
for n in range(1,h):
    l = []
    for k in range(n+1):
        tab[n][k] = tab[n-1][k-1]+tab[n-1][k] % 2
        l.append(0)


height = 600/h
width = 600/h
length = (window_height()-40)/h

def init_turtle():
    ht()
    pensize(1)
    tracer(False)
    speed(0)
    up()

def draw_square(posX,posY,clr="black"):
    color(clr)
    up()
    goto(posX-length/2,posY-length/2)
    down()
    begin_fill()
    for i in range(4):
        forward(length)
        left(90)
    end_fill()

print(time.time()-s)
print("drawing")
init_turtle()
for n,l in enumerate(tab):
    for i,v in enumerate(l):
        if v == 0: continue
        if v != 0:
            goto(i*length - n*length/2 , -n*length+window_height()/2-20)
            dot(2)
        update()


exitonclick()
