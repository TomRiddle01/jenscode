#!env python
from turtle import *
from math import *
import time



dt = 0.003 #[s]
perSecond = 1/dt

#k = 500000 #[N/m]


steps = [[]]
steps[0].append({})
steps[0][0]["mass"] = 10
steps[0][0]["stiffness"] = 500000
steps[0][0]["accel"] = 0 #klappt net
steps[0][0]["velocity"] = -10
steps[0][0]["distance"] = 0.1 #nicht ändern
steps[0][0]["f"] = lambda x, m, k: (k[0]*(x[1]-2*x[0]))/m

steps[0].append({})
steps[0][1]["mass"] = 10
steps[0][1]["stiffness"] = 500000
steps[0][1]["accel"] = 0 #klappt net
steps[0][1]["velocity"] = -10
steps[0][1]["distance"] = 0.2 #nicht ändern
steps[0][1]["f"] = lambda x, m, k: (k[1]*(x[0]-2*x[1]+x[2]))/m

steps[0].append({})
steps[0][2]["mass"] = 10
steps[0][2]["stiffness"] = 500000
steps[0][2]["accel"] = 0 #klappt net
steps[0][2]["velocity"] = -10
steps[0][2]["distance"] = 0.3 #nicht ändern
steps[0][2]["f"] = lambda x, m, k: (k[2]*(x[1]-x[2]+0.1))/m



def calculateContext(n, c0, z):
    c = {}
    c["mass"] = c0["mass"]
    c["stiffness"] = c0["stiffness"]
    f = c["f"] = c0["f"]
    distances = []
    for b in z:
        distances.append(b["distance"])
    stiffnesses = []
    for b in z:
        stiffnesses.append(b["stiffness"])
    if n!=0: 
        c["accel"] = f(distances, c["mass"], stiffnesses)
    else:
        c["accel"] = c0["accel"]
        
    c["velocity"] = c0["velocity"] + c["accel"] * dt
    c["distance"] = c0["distance"] + c["velocity"] * dt
    return c

def zeitschritt(n):
    for n in range(0,n):
        steps.append([])
        massID = 0
        for c in steps[n]:
            steps[n+1].append(calculateContext(n, c, steps[n]))
            massID += 1
        

zeitschritt(int(perSecond*10))


def d(x):
    return x*1500-300

def drawMass(masses, c):
    masses.goto(d(c["distance"]), 0)
    masses.dot(30)

def drawSine(t, p1, p2):
    t.goto(d(p1),0)
    for i in range(0, 100):
        t.down()
        x = d(p1+(i*(p2-p1)/100))
        y = sin(i/2)*15
        t.goto(x,y)
        t.up()



def animate():
    masses = Turtle()
    spring = Turtle()
    masses.color("black")
    spring.color("black")
    masses.up()
    spring.up()
    masses.ht()
    spring.ht()
    
    tracer(False)
    ht()
    up()
    goto(d(0), 1000)
    down()
    goto(d(0), -1000)
    up()
    tracer(True)

    for step in steps:
        tracer(False)
        masses.clear()
        spring.clear()
        before = 0
        for context in step:
            drawMass(masses, context)
            drawSine(spring, before, context["distance"])
            before = context["distance"]
        tracer(True)
        time.sleep(0.1)

ht()
up()
goto(window_width()/2-170, window_height()/2-20)
write("made by Yannis Rohloff")
goto(window_width()/2-170, window_height()/2-40)
write("with Jens' Senf")




animate()
input("pause")
