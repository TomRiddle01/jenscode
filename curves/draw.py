from curvelike import *
from turtle import *
import numpy as np
import sys
import pickle



name = "banana" if len(sys.argv)==1 else sys.argv[1]
(x, y) = curveLike(name)
kurve = Turtle()
kurve.ht()
kurve.pensize(2)
inkrement = 0.01
dots = "dots" in sys.argv
lines = "lines" in sys.argv
skipFrames = 0.007 # in percent (every nth Frame will be shown)
s = 1



if not(dots or lines):
    lines=True

listex = []
listex_diff=[]
listey = []
listey_diff=[]

def getFilename(text):
    filename = hashlib.sha256(text.encode("utf-8")).hexdigest()
    return "cache/"+re.sub('[^0-9a-zA-Z]+', '', text)+str(inkrement)+"_"+filename+".data.txt"

def findMax(i):
    growth = int(i/2)
    isnull = True
    while True:
        print("%d, %d"%(i*inkrement, growth))
        if y(i*inkrement)==0 and y((i-1)*inkrement) != 0:
            return i
        if y(i*inkrement)==0:
            i -= growth
            isnull_ = True
        else:
            i += growth
            isnull_ = False

        if isnull == isnull_:
            growth = int(growth*0.5)
            


maxI = findMax(1000)*inkrement

filename = getFilename(name)
if os.path.isfile(filename):
    (listex, listey) = pickle.load(open(filename, "rb"))
    print("Loaded Calculation from Cache")
else:
    print("Preparing, please wait")
    i=1
    nullcnt = 0
    while nullcnt < 10:
        print("Precalculating: %d%%" %(i*100/maxI))
        x_ = x(i)
        y_ = y(i)
        listex.append(x_)
        listey.append(y_)
        
        i+=inkrement
        if y_==0 and x_==0:
            nullcnt += 1
        else:
            nullcnt = 0
    print("Precalculating: %d%%" %(100))
    pickle.dump((listex, listey), open(filename, "wb"))


if lines:
    for j in range(0,len(listex)):
        if j<len(listex)-1:
            listex_diff.append(abs(listex[j+1]-listex[j]))
            listey_diff.append(abs(listey[j+1]-listey[j]))

    xmitteldiff = sum(listex_diff)/len(listex_diff)
    ymitteldiff = sum(listey_diff)/len(listey_diff)

width = max(listex)
height = max(listey)

s = min([150/width, 150/height])

kurve.up()


i = 0
nullcnt = 0
tracer(0,0)
l = len(listex)
for i in range(2, l):
    print("Progress: %d%%" %(i*100/l))
    xp = listex[i-1]
    x_ = listex[i]
    yp = listey[i-1]
    y_ = listey[i]
    
    if lines:
        if abs((x_-xp))<10*xmitteldiff and abs((y_-yp))<10*ymitteldiff:
            kurve.goto(s*x_,s*y_)
            kurve.down()
        else:
            kurve.up()
    if dots:
        kurve.goto(s*x_,s*y_)
        kurve.dot(3, "black")
    if i%int(skipFrames*l)==0:
        update()

print("Progress: %d%%" %(100))
exitonclick()




##x1 = (x(i+dx)-x(i))/dx
##y1 = (y(i+dx)-y(i))/dx
##
##if -30<x1<30 and -30<y1<30:
##    print("nicht regulär")
##else:
