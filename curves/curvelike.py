import re
import math
import cmath
import numpy
import wolframalpha
import hashlib
import os.path
import pickle

client = wolframalpha.Client("8TYVHQ-VAJRWJXPTW")

if not os.path.exists("cache"):
    os.makedirs("cache")

def getFilename(text):
    filename = hashlib.sha256(text.encode("utf-8")).hexdigest()
    return "cache/"+re.sub('[^0-9a-zA-Z]+', '', text)+"_"+filename+".txt"

def loadCurve(text):
    filename = getFilename(text)
    
    if os.path.isfile(filename):
        return pickle.load(open(filename, "rb"))
    else: 
        res = client.query(text + " curve")
        if res:
            for pod in res.pods:
                if pod.title == "Equations":
                    for subpod in pod.subpods:
                        if subpod.title == "Parametric equations":
                            pickle.dump(res, open(filename, "wb"))
                            return res
    exit("Could not process the Wolfram Alpha request")
    
def readResult(result):
    data = {}
    for pod in result.pods:
        if pod.title == "Equations":
            for subpod in pod.subpods:
                if subpod.title == "Parametric equations":
                    data["plaintext"] = subpod.plaintext
                    #data["width"] = subpod.width
                    #data["height"] = subpod.height
    return data


def csgn(z):
    if z == 0j:
        return z / abs(z)
    else:
        return 0j # todo

def curveLike(text):
    data = readResult(loadCurve(text))
    raw = data["plaintext"]
    # filter Functions
    m1 = re.search("x\(t\) = (.*)", raw)
    m2 = re.search("y\(t\) = (.*)", raw)
    if m1 and m2:
        xt = m1.group(1)
        yt = m2.group(1)
    else:
        print("no function found")
        exit(-1)

    # Replace Spaces with multiplication
    xt = xt.replace(" ", "*")
    yt = yt.replace(" ", "*")

    # add lambda

    xt = "func_xt = lambda t: "+xt
    yt = "func_yt = lambda t: "+yt


    # define used functions
    global func_xt, func_yt, theta, pi, sgn, sin, cos, sqrt
    theta = lambda x: 0 if complex(x).real <= 0 else 1
    sgn = numpy.sign
    sin = numpy.sin
    cos = numpy.cos
    pi = numpy.pi
    sqrt = numpy.sqrt

    exec(xt, globals())
    exec(yt, globals())
    return (func_xt, func_yt)

