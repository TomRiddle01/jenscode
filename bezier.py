from turtle import *
from math import *
import time
import numpy

verticies = []

def s(x):
    return x*1

bg = Turtle()
bg.ht()

def drawBackground():
    tracer(0)
    bg.color("grey")
    bg.pensize(3)
    bg.up()
    for x, y in verticies:
        x, y = s(x), s(y)
        bg.goto(x,y)
        bg.down()

    bg.up()
    bg.color("black")
    for x, y in verticies:
        x, y = s(x), s(y)
        bg.goto(x,y)
        bg.dot(6)
    tracer(1)


dots = Turtle()
dots.ht()

bezier = Turtle()
bezier.ht()
bezier.color("red")
bezier.pensize(4)

colors = ["green", "blue", "orange", "brown"]
def drawPoints(verts, color):
    dots.up()
    dots.color(color)
    dots.pensize(1)
    for x, y in verts:
        x, y = s(x), s(y)
        dots.goto(x,y)
        dots.dot()
        dots.down()

def getPointsBetween(verts, t):
    between = []
    for i in range(len(verts)-1):
        z1x, z1y = verts[i]
        z2x, z2y = verts[i+1]
        p = (z1x+(z2x-z1x)*t, z1y+(z2y-z1y)*t)
        between.append(p)
    return between


dst = 0
speed = 0.01 # per frame
hidden = False
# t should be passed time in percent from 0.0 to 1.0
def tick(t):
    global dst, speed
    dst += speed 
    if len(verticies) > 2:
        clear()
        dots.clear()
        time = dst
        between = verticies
        if time <= 1:
            while len(between) > 1:
                between = getPointsBetween(between,time)
                if not hidden:
                    drawPoints(between, colors[len(between)%len(colors)])
            bezier.down()
            bezier.goto(s(between[0][0]), s(between[0][1]))

        if time >= 1:
            bezier.goto(s(verticies[-1][0]), s(verticies[-1][1]))

ht()
st = time.time()

def replay():
    global st, dst
    st = time.time()
    bezier.clear()
    bezier.up()
    drawBackground()
    dst = 0
    bezier.up()
    bezier.goto(s(verticies[0][0]), s(verticies[0][1]))
def register_point(x, y):
    global st, dst
    verticies.append((x,y))
    st = time.time()
    bezier.clear()
    bezier.up()
    drawBackground()
    dst = 0
    bezier.up()
    bezier.goto(s(verticies[0][0]), s(verticies[0][1]))

def reset():
    global verticies, dst
    verticies = []
    bezier.clear()
    bezier.up()
    drawBackground()
    bg.clear()
    dots.clear()
    dst = 0

def hide():
    global hidden
    hidden = not hidden

screen = Screen()
screen.onkey(reset, "r")
screen.onkey(hide, "h")
screen.onkey(replay, "a")
screen.onscreenclick(register_point)


screen.listen()
while True:
    t = time.time()-st
    tracer(0)
    tick(t)
    tracer(1)

exitonclick()
