from math import factorial, ceil
from turtle import *

dim = 300
length= 2
nCr = lambda n,k: int(factorial(n)/(factorial(k)*factorial(n-k)))

ncrs = [[0]*dim]*dim
ncrss = {}
def init_turtle():
    tracer(False)
    ht()
    pensize(1)

def draw_square(posX,posY,clr="black"):
    color(clr)
    up()
    goto(posX-length/2,posY-length/2)
    down()
    begin_fill()
    for i in range(4):
        forward(length)
        left(90)
    end_fill()

def ncr(n,k):
    if k == 0 : return 1
    if k == n: return 1
    s = "%d.%d"%(n,k)
    if s in ncrss:
        return ncrss[s]
    e = ncr(n-1,k-1)+ncr(n-1,k)
    ncrss[s] = e
    return e

#MAIN
init_turtle()
for n in range(dim-1,0,-1):
    for k in range(n+1):
        if k > n//2:
            kk = n-k
        else:
            kk = k
        if ncr(n,kk) % 2 == 0:
            draw_square(k*length-n*length/2,-n*length+window_height()/2-length,"lightgrey")
        else:
            draw_square(k*length-n*length/2,-n*length+window_height()/2-length,"black")
exitonclick()
