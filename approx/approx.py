from turtle import *
from math import *
from sympy import *

x = symbols("x")

myfunc = [
        (lambda x: x==0, x),
        (lambda x: 0<x<=1, x*3),
        (lambda x: True, x)
        ]

def partial(funcdef, x):
    for (μ, λ) in funcdef:
        if μ(x):
            return λ

f = lambda x: partial(myfunc, x)

print(f(0))
print(f(1))
print(f(2))



exit()



tracer(False)

precision=40
x = symbols("x")
fourier = 0
A = 1
for i in range(1,60,2):
    fourier += (1/i)*sin(i*x)
fourier = ((4*A)/pi)*fourier

func = lambda stelle: fourier.evalf(subs={x:stelle})
scale_x = lambda s:s*3500-400
scale_y = lambda s:s*3500

up()
goto(scale_x(0),scale_y(0))
down()

for j in range(precision*13): #13 ungefähr = 4*pi
    if j <4*pi*precision:
        goto(scale_x(radians(j/float(precision))),scale_y(radians(func(j/float(precision)))))

ht()



tracer(True)




