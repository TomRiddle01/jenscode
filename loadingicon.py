from turtle import *
from math import *
import time
import numpy


st = time.time()
ht()

splits = 1
speed = 2
radius = 300
timesteps = 2

def tick(t):
    splits = min(20,int(t/timesteps)+1)
    for i in range(splits):
        distance = i*pi/splits
        z = (cos(distance)*radius, sin(distance)*radius)

        color("grey")
        up()
        goto(z[0], z[1])
        down()
        goto(-z[0], -z[1])
        up()
        color("black")
        t += pi/splits/speed
        d = (z[0]*sin(t*speed),z[1]*sin(t*speed))
        goto(d)
        dot(13)



while True:
    t = time.time()-st
    tracer(0)
    clear()
    tick(t)
    tracer(1)

exitonclick()
